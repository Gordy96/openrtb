package app

type App struct {
	collection map[string]interface{}
}

func (a App) Get(name string) interface{} {
	if r, p := a.collection[name]; p {
		return r
	}
	return nil
}

func (a App) Set(name string, s interface{}) {
	a.collection[name] = s
}

var app = App{make(map[string]interface{})}

func Instance() *App {
	return &app
}
