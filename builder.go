package main

import (
	"gopkg.in/mgo.v2"
	"strings"
)

type Builder interface {
	Where(key string , args... string) Builder
	Sort(key string, ord int) Builder
	Limit(count int) Builder
	Collection(name string) Builder
	DataBase(name string) Builder
	Connect(name string) Builder
	FindRaw(r map[string]interface{}) interface{}
	Get() interface{}
	//Projection()
}

type QueryBuilder struct {
	wheres map[string]interface{}
	orders map[string]int
	db *mgo.Database
	collection *mgo.Collection
	session *mgo.Session
	limit int
}

func (b *QueryBuilder) Sort(key string, ord int) Builder {
	b.orders[key] = ord
	return b
}

func (b *QueryBuilder) Where(key string , args... string) Builder {
	if val, ok := b.wheres[key]; ok {
		if l := len(args); l > 1 {
			switch args[l - 1] {
			case "or":
				switch v := val.(type) {
				case string:
					b.wheres[key] = map[string][]string {
						"or" : {v, args[0]},
					}
				case map[string][]string:
					v["or"] = append(v["or"], args[0])
					b.wheres[key] = v
				}
			case "in":
				switch v := val.(type) {
				case string:
					b.wheres[key] = map[string][]string {
						"in" : {v, args[0]},
					}
				case map[string][]string:
					v["in"] = append(v["in"], args[0])
					b.wheres[key] = v
				}
			}
			return b
		}
	}
	b.wheres[key] = args[0]
	return b
}

func (b *QueryBuilder) Limit(count int) Builder {
	b.limit = count
	return b
}

func (b *QueryBuilder) Collection(name string) Builder {
	b.collection = b.db.C(name)
	return b
}

func (b *QueryBuilder) Connect(name string) Builder {
	var e error
	b.session, e = mgo.Dial(name)
	if e != nil {
		panic(e)
	}
	return b
}

func (b *QueryBuilder) DataBase(name string) Builder {
	b.db = b.session.DB(name)
	return b
}

func (b *QueryBuilder) FindRaw(r map[string]interface{}) interface{} {
	var s []string = make([]string, 0)
	var o string
	for k, v := range b.orders {
		o = ""
		if v < 0 {
			o = "-"
		}
		s = append(s, o+k)
	}
	var res []interface{}
	q := b.collection.Find(r)
	if len(s) > 0 {
		q.Sort(strings.Join(s, ","))
	}
	q.Limit(b.limit).All(&res)
	return res
}

func (b *QueryBuilder) Get() interface{} {
	r := make(map[string]interface{})

	for k, v := range b.wheres {
		switch t := v.(type) {
		case string:
			r[k] = v
		case map[string][]string:
			for _k, _v := range t {
				switch _k {
				case "or":
					if _, pr := r["$or"]; !pr {
						r["$or"] = make([]map[string]string, 0)
					}

					for _, __v := range _v {
						__t := r["$or"].([]map[string]string)
						r["$or"] = append(__t, map[string]string {
							k:__v,
						})
					}
				case "in":
					r[k] = map[string][]string {
						"$in": v.([]string),
					}
				}
			}
		}
	}
	var s []string = make([]string, 0)
	var o string
	for k, v := range b.orders {
		o = ""
		if v < 0 {
			o = "-"
		}
		s = append(s, o+k)
	}
	var res []interface{}
	q := b.collection.Find(r)
	if len(s) > 0 {
		q.Sort(strings.Join(s, ","))
	}
	q.Limit(b.limit).All(&res)
	return res
}

func NewQueryNuilder() *QueryBuilder {
	b := QueryBuilder{}
	b.wheres = make(map[string]interface{})
	b.orders = make(map[string]int)
	b.limit = 25

	//session, err := mgo.Dial("127.0.0.1:27017")
	//if err != nil {
	//	panic(err)
	//}
	//defer session.Close()
	//
	//// Optional. Switch the session to a monotonic behavior.
	//session.SetMode(mgo.Monotonic, true)
	////c := session.DB("Test")

	return &b
}