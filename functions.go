package main

import (
	"github.com/bsm/openrtb"
	"gopkg.in/mgo.v2/bson"
	"errors"
	"strings"
	"openrtb/app"
	"gopkg.in/mgo.v2"
	"openrtb/entities"
)

func BannerSubQuery(banner *openrtb.Banner, find *bson.M, prefix string) error {
	if len(prefix) > 0 {
		prefix = prefix + "."
	}
	if len(banner.Format) > 0 {
		var restrictions = make([]bson.M,0)
		for _, f := range banner.Format {
			restrictions = append(restrictions, bson.M{prefix+"w":f.W, prefix+"h":f.H})
		}
		if banner.W > 0 && banner.H > 0{
			restrictions = append(restrictions, bson.M{prefix+"w":banner.W, prefix+"h":banner.H})
		}
		if and, p := (*find)["$and"]; p {
			and = append(and.([]bson.M), bson.M{"$or":restrictions})
			(*find)["$and"] = and
		} else {
			(*find)["$and"] = []bson.M{{"$or":restrictions}}
		}
	} else {
		(*find)[prefix+"w"] = banner.W
		(*find)[prefix+"h"] = banner.H
	}
	if len(banner.BType) > 0 {
		(*find)[prefix+"type"] = bson.M{
			"$nin": banner.BType,
		}
	}

	if len(banner.BAttr) > 0 {
		(*find)[prefix+"attr"] = bson.M{
			"$not": bson.M{
				"$elemMatch": bson.M{
					"$in": banner.BAttr,
				},
			},
		}
	}

	if banner.Pos > 0 {
		(*find)[prefix+"pos"] = banner.Pos
	}

	if len(banner.ExpDir) > 0 {
		(*find)[prefix+"expdir"] = bson.M{ "$in": banner.ExpDir }
	}

	if len(banner.Mimes) > 0 {
		(*find)[prefix+"mime"] = bson.M{ "$in": banner.Mimes }
	}

	if len(banner.Api) > 0 {
		(*find)[prefix+"api"] = bson.M{ "$in": banner.Api }
	}

	return nil
}

func VideoSubQuery(video *openrtb.Video, find *bson.M) error {
	if video.W > 0 && video.H > 0{
		(*find)["video.w"] = video.W
		(*find)["video.h"] = video.H
	}
	if video.MinDuration > 0 {
		(*find)["video.duration"] = bson.M{
			"$gte": video.MinDuration,
		}
	}

	if video.MaxDuration > 0 {
		if d, t := (*find)["video.duration"]; t {
			d.(bson.M)["$lte"] = video.MaxDuration
			(*find)["video.duration"] = d
		} else {
			(*find)["video.duration"] = bson.M{
				"$lte": video.MaxDuration,
			}
		}

	}

	if video.MinBitrate > 0 {
		(*find)["video.bitrate"] = bson.M{
			"$gte": video.MinBitrate,
		}
	}

	if video.MaxBitrate > 0 {
		if d, t := (*find)["video.bitrate"]; t {
			d.(bson.M)["$lte"] = video.MaxBitrate
			(*find)["video.bitrate"] = d
		} else {
			(*find)["video.bitrate"] = bson.M{
				"$lte": video.MaxBitrate,
			}
		}

	}

	if len(video.Protocols) > 0 {
		(*find)["video.protocol"] = bson.M{ "$in": video.Protocols }
	}

	if video.BoxingAllowed != nil {
		(*find)["video.boxingallowed"] = *video.BoxingAllowed
	}

	if len(video.PlaybackMethod) > 0 {
		(*find)["video.playbackmethod"] = bson.M{ "$in": video.PlaybackMethod }
	}

	if len(video.Mimes) > 0 {
		(*find)["video.mime"] = bson.M{ "$in": video.Mimes }
	}

	if video.Pos > 0 {
		(*find)["video.pos"] = video.Pos
	}

	if video.Placement > 0 {
		(*find)["video.placement"] = video.Placement
	}

	if len(video.Api) > 0 {
		(*find)["video.api"] = bson.M{ "$in": video.Api }
	}

	if len(video.Delivery) > 0 {
		(*find)["video.delivery"] = bson.M{ "$in": video.Delivery }
	}

	if len(video.BAttr) > 0 {
		(*find)["video.attr"] = bson.M{
			"$not": bson.M{
				"$elemMatch": bson.M{
					"$in": video.BAttr,
				},
			},
		}
	}

	if video.Sequence > 0 {
		(*find)["video.sequence"] = video.Sequence
	}

	if video.SkipAfter > 0 {
		(*find)["video.skipafter"] = video.SkipAfter
	}
	//How to check if skip is not important?!
	(*find)["video.skip"] = video.Skip

	if video.Linearity > 0 {
		(*find)["video.linearity"] = video.Linearity
	}

	if video.StartDelay > 0 {
		(*find)["video.startdelay"] = video.StartDelay
	}

	if cc := len(video.CompanionAd); cc > 0 {
		temp := make([]bson.M, cc)
		for i, companion := range video.CompanionAd {
			companionQuery := bson.M{}
			BannerSubQuery(&companion, &companionQuery, "")
			temp[i] = bson.M{"$elemMatch" : companionQuery }
		}
		(*find)["video.companions"] = bson.M{"$all" : temp}
	}

	return nil
}

func NativeSubQuery(native *openrtb.Native, find *bson.M) error {
	if len(native.API) > 0 {
		(*find)["native.api"] = bson.M{ "$in": native.API }
	}
	if len(native.BAttr) > 0 {
		(*find)["native.attr"] = bson.M{
			"$not": bson.M{
				"$elemMatch": bson.M{
					"$in": native.BAttr,
				},
			},
		}
	}
	if len(native.Ver) > 0 {
		(*find)["native.ver"] = native.Ver
	}
	return nil
}

func AudioSubQuery(audio *openrtb.Audio, find *bson.M) error {
	if audio.MinDuration > 0 {
		(*find)["audio.duration"] = bson.M{
			"$gte": audio.MinDuration,
		}
	}

	if audio.MaxDuration > 0 {
		if d, t := (*find)["audio.duration"]; t {
			d.(bson.M)["$lte"] = audio.MaxDuration
			(*find)["audio.duration"] = d
		} else {
			(*find)["audio.duration"] = bson.M{
				"$lte": audio.MaxDuration,
			}
		}

	}

	if audio.MinBitrate > 0 {
		(*find)["audio.bitrate"] = bson.M{
			"$gte": audio.MinBitrate,
		}
	}

	if audio.MaxBitrate > 0 {
		if d, t := (*find)["audio.bitrate"]; t {
			d.(bson.M)["$lte"] = audio.MaxBitrate
			(*find)["audio.bitrate"] = d
		} else {
			(*find)["audio.bitrate"] = bson.M{
				"$lte": audio.MaxBitrate,
			}
		}

	}

	if len(audio.Protocols) > 0 {
		(*find)["audio.protocol"] = bson.M{ "$in": audio.Protocols }
	}

	if len(audio.Mimes) > 0 {
		(*find)["audio.mime"] = bson.M{ "$in": audio.Mimes }
	}

	if len(audio.API) > 0 {
		(*find)["audio.api"] = bson.M{ "$in": audio.API }
	}

	if len(audio.Delivery) > 0 {
		(*find)["audio.delivery"] = bson.M{ "$in": audio.Delivery }
	}

	if len(audio.BAttr) > 0 {
		(*find)["audio.attr"] = bson.M{
			"$not": bson.M{
				"$elemMatch": bson.M{
					"$in": audio.BAttr,
				},
			},
		}
	}

	if audio.Sequence > 0 {
		(*find)["audio.sequence"] = audio.Sequence
	}

	if audio.StartDelay > 0 {
		(*find)["audio.startdelay"] = audio.StartDelay
	}

	if audio.Feed > 0 {
		(*find)["audio.feed"] = audio.Feed
	}

	if audio.NVol > 0 {
		(*find)["audio.nvol"] = audio.NVol
	}

	if audio.Stitched > 0 {
		(*find)["audio.stitched"] = audio.Stitched
	}

	return nil
}

func ScanSiteCategories(categories []string, find *bson.M, fieldName string) (error) {
	totalCategories := make([]string,0)
	temp := make(map[string]bool)
	if len(categories) == 0 {
		return errors.New("no categories")
	}
	for _, c := range categories {
		if _, b := temp[c]; !b {
			totalCategories = append(totalCategories, c)
			superCategory := strings.Split(c, "-")[0]
			temp[c] = true
			if _, b := temp[superCategory]; !b {
				totalCategories = append(totalCategories, superCategory)
				temp[superCategory] = true
			}
		}
	}
	if len(totalCategories) > 0 {
		(*find)[fieldName] = bson.M{ "$elemMatch" : bson.M{ "$in" : totalCategories } }
		return nil
	}
	return errors.New("no categories")
}

func ScanSiteKeywords(keywords []string, find *bson.M, project *bson.M, sort *[]string) (error) {
	if len(keywords) == 0 {
		return errors.New("empty array")
	}
	keywords = append(keywords, "_")
	query := strings.Join(keywords, " ")
	(*find)["$text"] = bson.M{"$search" : query}
	(*project)["score"] = bson.M{"$meta": "textScore"}
	*sort = append(*sort, "$textScore:score")
	return nil
}

func GetCountry(geo *openrtb.Geo) []string{
	col := (app.Instance().Get("dbcon").(*mgo.Session)).DB("Test").C("Countries")
	var res = make([]*entities.Country,0)
	col.Find(bson.M{
		"geometry": bson.M{
			"$geoIntersects": bson.M{
				"$geometry": bson.M{
					"type": "Point" ,
					"coordinates": []float64{geo.Lon , geo.Lat},
				},
			},
		},
	}).All(&res)
	ids := make([]string,0)
	for _, t := range res {
		ids = append(ids, t.Code)
	}
	return ids
}

func InCountry(find *bson.M, callback func(*openrtb.Geo)[]string, geo *openrtb.Geo) error {
	countries := callback(geo)
	if len(countries) == 0 {
		return errors.New("country not found")
	}

	restrictions := []bson.M{
		{ "countries": []bson.M{} },
		{ "countries": bson.M{ "$elemMatch": bson.M{ "$in": countries } } },
	}
	if and, p := (*find)["$and"]; p {
		and = append(and.([]bson.M), bson.M{"$or":restrictions})
		(*find)["$and"] = and
	} else {
		(*find)["$and"] = []bson.M{{"$or":restrictions}}
	}
	return nil
}