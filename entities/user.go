package entities

type User struct {
	BuyerIDS   []string   `json:"buyerids,omitempty"`    // Buyer-specific ID for the user as mapped by the exchange for the buyer. At least one of buyeruid/buyerid or id is recommended. Valid for OpenRTB 2.3.
	YOB        int       `json:"yob,omitempty"`        // Year of birth as a 4-digit integer.
	Gender     string    `json:"gender,omitempty"`     // Gender ("M": male, "F" female, "O" Other)
	Keywords   string    `json:"keywords,omitempty"`   // Comma separated list of keywords, interests, or intent
	CustomData string    `json:"customdata,omitempty"` // Optional feature to pass bidder data that was set in the exchange's cookie. The string must be in base85 cookie safe characters and be in any format. Proper JSON encoding must be used to include "escaped" quotation marks.
	Data       []Data    `json:"data,omitempty"`
}
