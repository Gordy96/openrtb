package entities

import "encoding/json"

type Audio struct {
	Mime         	string  	`json:"mime,omitempty"`                 // Content MIME types supported.
	Duration   		int       	`json:"duration,omitempty"` // Minimum video ad duration in seconds
	Protocol     	int     	`json:"protocol,omitempty"`   // Video bid response protocols
	StartDelay    	int       	`json:"startdelay,omitempty"`  // Indicates the start delay in seconds
	Sequence      	int       	`json:"sequence,omitempty"`    // Default: 1
	Attr         	[]int     	`json:"attr,omitempty"`       // Blocked creative attributes
	Bitrate    		int       	`json:"bitrate,omitempty"`  // Minimum bit rate in Kbps
	Delivery    	int     	`json:"delivery,omitempty"`    // List of supported delivery methods
	Api           	int     	`json:"api,omitempty"`
	MaxSequence   	int       	`json:"maxseq,omitempty"`   // The maximumnumber of ads that canbe played in an ad pod.
	Feed          	int       	`json:"feed,omitempty"`     // Type of audio feed.
	Stitched      	int       	`json:"stitched,omitempty"` // Indicates if the ad is stitched with audio content or delivered independently
	NVol          	int       	`json:"nvol,omitempty"`     // Volume normalization mode.
}

type jsonAudio Audio

func (a *Audio) Validate() error {
	return nil
}

func (a *Audio) MarshalJSON() ([]byte, error) {
	a.normalize()
	return json.Marshal((*jsonAudio)(a))
}

func (a *Audio) UnmarshalJSON(data []byte) error {
	var h jsonAudio
	if err := json.Unmarshal(data, &h); err != nil {
		return err
	}

	*a = (Audio)(h)
	a.normalize()
	return nil
}

func (a *Audio) normalize() {
	if a.Sequence == 0 {
		a.Sequence = 1
	}
}
