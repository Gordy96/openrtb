package entities

import "gopkg.in/mgo.v2/bson"

type Country struct {
	Id			bson.ObjectId	`json:"id" bson:"_id,omitempty"`
	Code		string 			`json:"code"`
	Name		string			`json:"name"`
	Geometry	*Location		`json:"geometry,omitempty"`
}
