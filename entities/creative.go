package entities

import "gopkg.in/mgo.v2/bson"

type Creative struct {
	ID          		bson.ObjectId	`json:"id" bson:"_id,omitempty"` 				// Unique ID of the bid request
	Banner            	*Banner   		`json:"banner,omitempty"`
	Video             	*Video    		`json:"video,omitempty"`
	Audio             	*Audio    		`json:"audio,omitempty"`
	Native            	*Native   		`json:"native,omitempty"`
	Instl             	int            	`json:"instl,omitempty"`             // Interstitial, Default: 0 ("1": Interstitial, "0": Something else)
	TagID             	string			`json:"tagid,omitempty"`             // IDentifier for specific ad placement or ad tag
	Secure            	int				`json:"secure,omitempty"`            // Flag to indicate whether the impression requires secure HTTPS URL creative assets and markup.
	Site        		*Site			`json:"site,omitempty"`
	App         		*App			`json:"app,omitempty"`
	Device      		*Device     	`json:"device,omitempty"`
	User        		*User			`json:"user,omitempty"`
	WLang       		[]string     	`json:"wlang,omitempty"`   	// Array of languages for creatives using ISO-639-1-alpha-2
	Cur         		string     		`json:"cur,omitempty"`     	// Array of allowed currencies
	Price				float64			`json:"price,omitempty"`
	Countries			[]string		`json:"Countries,omitempty"`
	Hours				HoursPair		`json:"hours,omitempty"`
	Days				[]int			`json:"days,omitempty"`
	Cat           		[]string   		`json:"cat,omitempty"`
	Source				string			`json:"source,omitempty"`
}

