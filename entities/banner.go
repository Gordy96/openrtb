package entities

type Banner struct {
	W        	int       	`json:"w,omitempty"`        // Width
	H        	int       	`json:"h,omitempty"`        // Height
	Type    	int     	`json:"type,omitempty"`    // Blocked creative types
	Attr    	[]int     	`json:"attr,omitempty"`    // Blocked creative attributes
	Pos      	int       	`json:"pos,omitempty"`      // Ad Position
	Mime    	string  	`json:"mime,omitempty"`    // Whitelist of content MIME types supported
	TopFrame 	int      	`json:"topframe,omitempty"` // Default: 0 ("1": Delivered in top frame, "0": Elsewhere)
	ExpDir   	int     	`json:"expdir,omitempty"`   // Specify properties for an expandable ad
	Api      	int     	`json:"api,omitempty"`      // List of supported API frameworks
	Vcm			*int		`json:"vcm,omitempty"`
}
