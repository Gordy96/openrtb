package entities

import (
	"errors"
	"encoding/json"
)

const (
	LocationTypePoint = "Point"
	LocationTypeLineString = "LineString"
	LocationTypePolygon = "Polygon"
	LocationTypeMultiPoint = "MultiPoint"
	LocationTypeMultiLineString = "MultiLineString"
	LocationTypeMultiPolygon = "MultiPolygon"
	LocationTypeGeometryCollection = "GeometryCollection"
)

type Location struct {
	Type string							`json:"type"`
	Coordinates [][][2]float64			`json:"coordinates"`
}

type locationAlias Location

func def(t string) string {
	switch t {
	case LocationTypePoint,
		LocationTypeLineString,
		LocationTypePolygon,
		LocationTypeMultiPoint,
		LocationTypeMultiLineString,
		LocationTypeMultiPolygon,
		LocationTypeGeometryCollection:
		return t
	}
	return LocationTypePoint
}

func (l *locationAlias) normalize() {
	l.Type = def(l.Type)
}

func (l *Location) UnmarshalJSON(data []byte) error {
	test := &locationAlias{
		Type: LocationTypeMultiPoint,
	}

	_ = json.Unmarshal(data, test)

	test.normalize()

	*l = Location(*test)
	return nil
}

type Extension []byte

func (e Extension) MarshalJSON() ([]byte, error) {
	//t := []byte(`"`)
	//t = append(t, e...)
	//t = append(t, '"')
	//e = t
	return e, nil
}

func (e *Extension) UnmarshalJSON(data []byte) error {
	if e == nil {
		return errors.New("openrtb.Extension: UnmarshalJSON on nil pointer")
	}
	//if data[0] == '\'' || data[0] == '"' {
	//	data = data[1:len(data) - 1]
	//}
	*e = append((*e)[0:0], data...)
	return nil
}

type Data struct {
	ID      string    `json:"id,omitempty"`
	Name    string    `json:"name,omitempty"`
	Segment []Segment `json:"segment,omitempty"`
}

type Segment struct {
	ID    []string		`json:"id,omitempty"`
	Name  string    	`json:"name,omitempty"`
	Value string    	`json:"value,omitempty"`
}

type ThirdParty struct {
	ID     string		`json:"id,omitempty"`
	Name   string		`json:"name,omitempty"`
	Cat    []string		`json:"cat,omitempty"` // Array of IAB content categories
	Domain string		`json:"domain,omitempty"`
}

// The publisher object itself and all of its parameters are optional, so default values are not
// provided. If an optional parameter is not specified, it should be considered unknown.
type Publisher ThirdParty

// The producer is useful when content where the ad is shown is syndicated, and may appear on a
// completely different publisher. The producer object itself and all of its parameters are optional,
// so default values are not provided. If an optional parameter is not specified, it should be
// considered unknown.
type Producer ThirdParty

type HoursPair struct {
	From		int		`json:"from"`
	Until		int		`json:"until"`
}