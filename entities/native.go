package entities

type Native struct {
	Request Extension `json:"request"`         // Request payload complying with the Native Ad Specification.
	Ver     string    `json:"ver,omitempty"`   // Version of the Native Ad Specification to which request complies; highly recommended for efficient parsing.
	Api     int     `json:"api,omitempty"`   // List of supported API frameworks for this impression.
	Attr   	[]int     `json:"attr,omitempty"` // Blocked creative attributes
}

