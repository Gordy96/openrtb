package entities

type Device struct {
	//IP         string    `json:"ip,omitempty"`             // IPv4
	//IPv6       string    `json:"ipv6,omitempty"`           // IPv6
	DeviceTypes []int       `json:"devicetypes,omitempty"`     // The general type of device.
	//Make       string    `json:"make,omitempty"`           // Device make
	//Model      string    `json:"model,omitempty"`          // Device model
	OS         []string    `json:"os,omitempty"`             // Device OS
	//OSVer      string    `json:"osv,omitempty"`            // Device OS version
	//HwVer      string    `json:"hwv,omitempty"`            // Hardware version of the device (e.g., "5S" for iPhone 5S).
	H          int       `json:"h,omitempty"`              // Physical height of the screen in pixels.
	W          int       `json:"w,omitempty"`              // Physical width of the screen in pixels.
	PPI        int       `json:"ppi,omitempty"`            // Screen size as pixels per linear inch.
	PxRatio    float64   `json:"pxratio,omitempty"`        // The ratio of physical pixels to device independent pixels.
	JS         int       `json:"js,omitempty"`             // Javascript status ("0": Disabled, "1": Enabled)
	//GeoFetch   int       `json:"geofetch,omitempty"`       // Indicates if the geolocation API will be available to JavaScript code running in the banner,
	FlashVer   string    `json:"flashver,omitempty"`       // Flash version
	Language   string    `json:"language,omitempty"`       // Browser language
	Carrier    string    `json:"carrier,omitempty"`        // Carrier or ISP derived from the IP address
	//MCCMNC     []string  `json:"mccmnc,omitempty"`         // Mobile carrier as the concatenated MCC-MNC code (e.g., "310-005" identifies Verizon Wireless CDMA in the USA).
	ConnType   int       `json:"connectiontype,omitempty"` // Network connection type.
	//IFA        string    `json:"ifa,omitempty"`            // Native identifier for advertisers
}