package entities

import (
	"encoding/json"
)

type Inventory struct {
	ID            []string     `json:"id,omitempty"` // ID on the exchange
	Name          []string     `json:"name,omitempty"`
	Domain        []string     `json:"domain,omitempty"`
	Cat           []string   `json:"cat,omitempty"`           // Array of IAB content categories
	SectionCat    []string   `json:"sectioncat,omitempty"`    // Array of IAB content categories for subsection
	PageCat       []string   `json:"pagecat,omitempty"`       // Array of IAB content categories for page
	PrivacyPolicy int       `json:"privacypolicy,omitempty"` // Default: 1 ("1": has a privacy policy)
	Publisher     *Publisher `json:"publisher,omitempty"`     // Details about the Publisher
	Content       *Content   `json:"content,omitempty"`       // Details about the Content
	Keywords      []string     `json:"keywords,omitempty"`      // Comma separated list of keywords about the site.
}

type jsonInventory Inventory

func (i *Inventory) MarshalJSON() ([]byte, error) {
	i.normalize()
	return json.Marshal((*jsonInventory)(i))
}

// UnmarshalJSON custom unmarshalling with normalization
func (i *Inventory) UnmarshalJSON(data []byte) error {
	var h jsonInventory
	if err := json.Unmarshal(data, &h); err != nil {
		return err
	}

	*i = (Inventory)(h)
	i.normalize()
	return nil
}

func (i *Inventory) normalize() {
	var found bool = false
	for _, w := range i.Keywords {
		if w == "_" {
			found = true
		}
	}
	if !found {
		i.Keywords = append(i.Keywords, "_")
	}
}

// An "app" object should be included if the ad supported content is part of a mobile application
// (as opposed to a mobile website).  A bid request must not contain both an "app" object and a
// "site" object.
type App struct {
	Inventory
	Bundle   string `json:"bundle,omitempty"`   // App bundle or package name
	StoreURL string `json:"storeurl,omitempty"` // App store URL for an installed app
	Ver      string `json:"ver,omitempty"`      // App version
	Paid     int    `json:"paid,omitempty"`     // "1": Paid, "2": Free
}

// A site object should be included if the ad supported content is part of a website (as opposed to
// an application).  A bid request must not contain both a site object and an app object.
type Site struct {
	Inventory
	Page   string `json:"page,omitempty"`   // URL of the page
	Mobile int    `json:"mobile,omitempty"` // Mobile ("1": site is mobile optimised)
}
