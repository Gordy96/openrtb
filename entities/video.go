package entities

import (
	"encoding/json"
	"github.com/bsm/openrtb"
)

type Video struct {
	W              	int       	`json:"w,omitempty"`              // Width of the player in pixels
	H              	int       	`json:"h,omitempty"`              // Height of the player in pixels
	Mime          	string  	`json:"mime,omitempty"`          // Content MIME types supported.
	Duration    	int       	`json:"duration,omitempty"`    // Minimum video ad duration in seconds
	Protocol      	int     	`json:"protocol,omitempty"`      // Video bid response protocols
	BoxingAllowed	*int		`json:"boxingallowed,omitempty"`
	StartDelay     	int       	`json:"startdelay,omitempty"`     // Indicates the start delay in seconds
	Linearity      	int       	`json:"linearity,omitempty"`      // Indicates whether the ad impression is linear or non-linear
	Skip           	int       	`json:"skip,omitempty"`           // Indicates if the player will allow the video to be skipped, where 0 = no, 1 = yes.
	SkipAfter      	int       	`json:"skipafter,omitempty"`      // Number of seconds a video must play before skipping is enabled
	Attr          	[]int     	`json:"attr,omitempty"`          // Blocked creative attributes
	Bitrate     	int       	`json:"bitrate,omitempty"`     // Minimum bit rate in Kbps
	PlaybackMethod 	int 	  	`json:"playbackmethod,omitempty"` // List of allowed playback methods
	Delivery       	int			`json:"delivery,omitempty"`       // List of supported delivery methods
	Pos            	int       	`json:"pos,omitempty"`            // Ad Position
	Api             int     	`json:"api,omitempty"` // List of supported API frameworks
	Placement      	int       	`json:"placement,omitempty"` // Video placement type
	Sequence		int			`json:"sequence,omitempty"`
	Companions		[]Banner	`json:"companions,omitempty"`
}

type jsonVideo Video

func (v *Video) GetBoxingAllowed() int {
	return *v.BoxingAllowed
}

func (v *Video) MarshalJSON() ([]byte, error) {
	v.normalize()
	return json.Marshal((*jsonVideo)(v))
}

// UnmarshalJSON custom unmarshalling with normalization
func (v *Video) UnmarshalJSON(data []byte) error {
	var h jsonVideo
	if err := json.Unmarshal(data, &h); err != nil {
		return err
	}

	*v = (Video)(h)
	v.normalize()
	return nil
}

func (v *Video) normalize() {
	if v.Sequence == 0 {
		v.Sequence = 1
	}
	if v.Linearity == 0 {
		v.Linearity = openrtb.VideoLinearityLinear
	}
}
