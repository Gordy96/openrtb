package main

import (
	"gopkg.in/mgo.v2/bson"
	"encoding/json"
	"github.com/bsm/openrtb"
	"net/http"
	"gopkg.in/mgo.v2"
	"openrtb/app"
	"openrtb/entities"
	"os"
	"bytes"
	"io"
	"flag"
	"strings"
	"log"
)

type Server struct {
}

const (
	IMP_BANNER = iota
	IMP_VIDEO = iota
	IMP_AUDIO = iota
	IMP_NATIVE = iota
)

func Sort(fields ...string) bson.D {
	var order bson.D
	for _, field := range fields {
		n := 1
		var kind string
		if field != "" {
			if field[0] == '$' {
				if c := strings.Index(field, ":"); c > 1 && c < len(field)-1 {
					kind = field[1:c]
					field = field[c+1:]
				}
			}
			switch field[0] {
			case '+':
				field = field[1:]
			case '-':
				n = -1
				field = field[1:]
			}
		}
		if field == "" {
			panic("Sort: empty field name")
		}
		if kind == "textScore" {
			order = append(order, bson.DocElem{field, bson.M{"$meta": kind}})
		} else {
			order = append(order, bson.DocElem{field, n})
		}
	}
	return order
}

func bidRequest (writer http.ResponseWriter, request *http.Request) {
	col := (app.Instance().Get("dbcon").(*mgo.Session)).DB("Test").C("Creatives")
	var req = openrtb.BidRequest{}
	json.NewDecoder(request.Body).Decode(&req)

	//make a query to filter by not strict credentials like app, site, user, device
	var find = bson.M{}
	var project = bson.M{}

	var sort = make([]string, 0)
	//sort[0] = "-price"


	project["win_score"] = bson.M{"$multiply" : []interface{}{"$price", "$wprc"}}
	sort = append(sort, "-win_score")

	//source zone
	if req.Site != nil {
		ScanSiteCategories(req.Site.Cat, &find, "site.inventory.cat")
		ScanSiteKeywords(strings.Split(req.Site.Keywords, ","), &find, &project, &sort)
	} else if req.App != nil {
		ScanSiteCategories(req.App.Cat, &find, "app.inventory.cat")
		ScanSiteKeywords(strings.Split(req.App.Keywords, ","), &find, &project, &sort)
	}

	//device zone
	if req.Device != nil {
		if req.Device.DeviceType > 0 {
			find["device.devicetypes"] = bson.M{
				"$elemMatch": bson.M{
					"$eq": req.Device.DeviceType,
				},
			}
		}
		if len(req.Device.OS) > 0 {
			find["device.os"] = bson.M{
				"$elemMatch": bson.M{
					"$eq": req.Device.OS,
				},
			}
		}
		if req.Device.Geo != nil {
			InCountry(&find, GetCountry, req.Device.Geo)
		}

	}


	var bids = make([]openrtb.Bid, 0)

	for _, imp := range req.Imp {
		var sq = bson.M{}
		var ty int
		if imp.Banner != nil {
			BannerSubQuery(imp.Banner, &sq, "banner")
			ty = IMP_BANNER
		} else if imp.Video != nil {
			VideoSubQuery(imp.Video, &sq)
			ty = IMP_VIDEO
		} else if imp.Native != nil {
			NativeSubQuery(imp.Native, &sq)
			ty = IMP_NATIVE
		} else if imp.Audio != nil {
			AudioSubQuery(imp.Audio, &sq)
			ty = IMP_AUDIO
		}

		var res entities.Creative
		for k, v := range find {
			if k == "$and" {
				if and, p := sq["$and"]; p {
					switch t := v.(type){
					case bson.M:
						and = append(and.([]bson.M), t)
					case []bson.M:
						for _, v := range t {
							and = append(and.([]bson.M), v)
						}
					}
					sq["$and"] = and
				} else {
					sq["$and"] = v
				}
			} else {
				sq[k] = v
			}
		}
		sort_o := Sort(sort...)
		query := col.Pipe([]bson.M{{"$match":sq},{"$addFields":project},{"$sort":sort_o}})//.Limit(20).Select(project).Sort(sort...)
		j, _ := json.Marshal([]bson.M{{"$match":sq},{"$addFields":project},{"$sort":sort_o}})
		log.Printf("%s\n", j)
		err := query.One(&res)
		if err == nil {
			bid := openrtb.Bid{}
			bid.ImpID = imp.ID
			bid.ID = res.ID.Hex()
			bid.AdMarkup = res.Source
			bid.Price = res.Price
			switch ty {
			case IMP_BANNER:
				bid.W = res.Banner.W
				bid.H = res.Banner.H
			case IMP_VIDEO:
				bid.W = res.Video.W
				bid.H = res.Video.H
			}
			bid.NURL = request.URL.Hostname() + "/ad/" + res.ID.Hex()
			bids = append(bids, bid)
		} else {
			panic(err)
		}
	}
	writer.Header().Set("content-type", "application/json")
	response := openrtb.BidResponse{}
	response.ID = bson.NewObjectId().Hex()
	var status = http.StatusNoContent
	if len(bids) > 0 {
		response.SeatBid = []openrtb.SeatBid{openrtb.SeatBid{Bid:bids}}
		status = http.StatusOK
	}
	bytes, _ := json.Marshal(response)
	writer.WriteHeader(status)
	writer.Write(bytes)
}

func (s Server) ServeHTTP(writer http.ResponseWriter, request *http.Request) {
	if request.Method == "POST" && request.URL.Path == "/" {
		bidRequest(writer, request)
	} else if request.Method == "POST" && request.URL.Path == "/save" {
		col := (app.Instance().Get("dbcon").(*mgo.Session)).DB("Test").C("Creatives")
		cr := entities.Creative{}
		err := json.NewDecoder(request.Body).Decode(&cr)
		err = col.Insert(cr)

		if err != nil {
			bytes, _ := json.Marshal(map[string]interface{}{"success":false,"error": err.Error()})
			writer.Write(bytes)
		} else {
			bytes, _ := json.Marshal(map[string]interface{}{"success":true})
			writer.Write(bytes)
		}
	} else if request.Method == "GET" && request.URL.Path == "/" {
		writer.Header().Set("Content-Type", "text/html; charset=utf-8")
		f, e := os.Open("index.html")
		if e != nil {
			panic(e)
		}
		buf := bytes.NewBuffer(nil)
		io.Copy(buf, f)
		f.Close()
		writer.Write(buf.Bytes())
	}
}

func main() {
	handler := Server{}
	var local bool
	var port string
	var dbaddr string
	var dbport string

	flag.BoolVar(&local, "local", false, "should listen local address connections or all")
	flag.StringVar(&port, "port", "8080", "port address to listen")
	flag.StringVar(&dbaddr, "dbaddr", "127.0.0.1", "database address")
	flag.StringVar(&dbport, "dbport", "27017", "database port")

	flag.Parse()

	session, err := mgo.Dial(dbaddr + ":" + dbport)
	if err != nil {
		log.Print(err)
	}
	app.Instance().Set("dbcon", session)
	addrtolisten := "0.0.0.0"
	if local {
		addrtolisten = "127.0.0.1"
	}

	err = http.ListenAndServe(addrtolisten + ":" + port, handler)
	if err != nil {
		log.Print(err)
	}
	defer app.Instance().Get("dbcon").(*mgo.Session).Close()
}